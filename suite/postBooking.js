const { expect } = require("chai");
const user = require("../data/user.json");
const {CreateBooking} = require("../service/createBooking");



describe("Create Booking on reqres", () => {
  let resBooking 
  before(async () => {
    resBooking =  await CreateBooking() 
  })
  
  it("Response status is 200",async () => {
    expect(resBooking.status).to.equal(201);
  });
  
  it("Name is morpheus", async () => {
    expect(resBooking.body.name).to.includes(user.name);
  });
})