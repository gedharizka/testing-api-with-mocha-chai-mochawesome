const request = require("supertest");
const user = require("../data/user.json");

async function CreateBooking  () {
  return await request("https://reqres.in").post("/api/users").send(user).set('Accept', 'application/json')
}

module.exports={
  CreateBooking
}