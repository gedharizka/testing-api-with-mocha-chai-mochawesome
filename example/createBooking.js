const request = require("supertest");
const { expect } = require("chai");

// Get booking
describe("Create Booking on reqres", () => {
  const response = request("https://reqres.in").post("/api/users").send({
    name: "morpheus",
    job: "leader",
  });
  it("Response status is 200", async () => {
    expect((await response).status).to.equal(201);
  });
  it("name is morpheus", async () => {
    expect((await response).body.name).to.includes("morpheus");
  });
});

