const request = require('supertest');
const {expect} = require('chai')

// Get booking
describe("Get Booking", ()=>{
  it("Response status is 200",async ()=> {
    const response = await request('https://restful-booker.herokuapp.com').get("/booking")
    // console.log(JSON.stringify(response))
    // assertion
    expect(response.status).to.equal(200);
    // expect(response.body).to.include("166","include booking id")
  })
})