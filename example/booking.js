const request = require("supertest");
const { expect } = require("chai");
const user = require("../data/user.json");

// Get booking

const CreateBooking = async () => {
  return await request("https://reqres.in").post("/api/users").send(user).set('Accept', 'application/json')
}

describe("Create Booking on reqres", async() => {
  let response;

  before(async () => {
    response = await CreateBooking();
  })
  
  it("Response status is 200", async () => {
    expect(response.status).to.equal(201);
  });

  it("Name is morpheus", async () => {
    expect(response.body.name).to.includes(user.name);
  });
 
});
